﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static MIDD_LEGACY.Model.transferOnline;

namespace MIDD_LEGACY.Util
{
    public class PG_Client
    {
        ClassUtil util = new ClassUtil();
        public transferOnlineInquiryResponse transferOnlineInquiry(transferOnlineInquiryRequest inquiryRequest)
        {
            transferOnlineInquiryResponse transferOnlineInquiryResponse = new transferOnlineInquiryResponse();

            string json = util.SendHTTP(inquiryRequest, "http://10.99.0.53:5555/invoke/raya.transfer.outgoing.providers:inquiry");

            if (json != "ERROR")
            { 
                transferOnlineInquiryResponse = JsonSerializer.DeserializeFromString<transferOnlineInquiryResponse>(json);
            }
            return transferOnlineInquiryResponse;
        }

        public transferOnlinePaymentResponse transferOnline(transferOnlinePaymentRequest transferRequest)
        {
            transferOnlinePaymentResponse transferResponse = new transferOnlinePaymentResponse();

            string json = util.SendHTTP(transferRequest, "http://10.99.0.53:5555/invoke/raya.transfer.outgoing.providers:transfer");

            if (json != "ERROR")
            {
                transferResponse = JsonSerializer.DeserializeFromString<transferOnlinePaymentResponse>(json);
            }
            return transferResponse;
        }
    }
}