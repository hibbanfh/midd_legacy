﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIDD_LEGACY.Util
{
    public class ClassConstant
    {
        public static string respCode00 = "00";
        public static string respCode01 = "01";
        public static string respCode02 = "02";
        public static string respCode03 = "03";
        public static string respCodeRev = "XX";
        public static string respLoginFailed = "Wrong User/Password";
        public static string respAlreadyLogin = "User sedang login di perangkat lain";
        public static string respLoginSuccess = "Login success";
        public static string respgetDataSuccess = "Get data success";
        public static string respgetDataFail = "Get data failed";
        public static string respTrxSuccess = "Transaction success";
        public static string respTrxFail = "Transaction failed";
        public static string respOpenAcctSuccess = "Open account success";
        public static string respSessionExpired = "Session Expired";
        public static string respUserWrong = "Username or Password not valid";
        public static string respTokenExpired = "Token Expired";
        public static string GrantType = "password";
        public static string ResponseError = "ERROR";
    }
}