﻿using RestSharp;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIDD_LEGACY.Util
{
    public class ClassUtil
    {
        public string SendHTTP(object reqModel, string url)
        {

             string jsonResponse = "";

            try
            {
                string jsonRequest = JsonSerializer.SerializeToString(reqModel);
                var client = new RestClient(url);
                var request = new RestRequest(url, Method.Post);

                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("authorization", "Basic QWRtaW5pc3RyYXRvcjptYW5hZ2U=");

                request.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);

                RestResponse response = client.Execute(request);
                jsonResponse = response.Content;

                if (jsonResponse == null || jsonResponse == "" || response.StatusCode.ToString() != "OK")
                {
                    throw new Exception("Response Error From API Target " + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                jsonResponse = ClassConstant.ResponseError;
            }


            return jsonResponse;
        }
    }
}