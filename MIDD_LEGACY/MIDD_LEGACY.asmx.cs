﻿using MIDD_LEGACY.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using static MIDD_LEGACY.Model.transferOnline;

namespace MIDD_LEGACY
{
    /// <summary>
    /// Summary description for MIDD_LEGACY
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MIDD_LEGACY : System.Web.Services.WebService
    {
        PG_Client pg = new PG_Client();

        [WebMethod]
        public transferOnlineInquiryResponse TransferOnlineInquiry(transferOnlineInquiryRequest inquiryRequest)
        {
            transferOnlineInquiryResponse transferOnlineInquiry = new transferOnlineInquiryResponse();

            transferOnlineInquiry = pg.transferOnlineInquiry(inquiryRequest);
            return transferOnlineInquiry;
        }


        [WebMethod]
        public transferOnlinePaymentResponse TransferOnlinePayment(transferOnlinePaymentRequest transfer)
        {
            transferOnlinePaymentResponse transferResponse = new transferOnlinePaymentResponse();
            transferResponse = pg.transferOnline(transfer);
            return transferResponse;
        }
    }
}
