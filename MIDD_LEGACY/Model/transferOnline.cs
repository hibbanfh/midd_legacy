﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIDD_LEGACY.Model
{
    public class transferOnline
    {

        #region INQUIRY
        public class transferOnlineCoreInfoInquiryRequest
        {
            public string SourceAccount { get; set; }
            public string Amount { get; set; }
        }

        public class transferOnlineThirdPartyInfoInquiryRequest
        {
            public string DestinationAccount { get; set; }
            public string BankCode { get; set; }
        }

        public class transferOnlineInquiryRequest
        {
            public transferOnlineCoreInfoInquiryRequest CoreInfo { get; set; }
            public transferOnlineThirdPartyInfoInquiryRequest ThirdPartyInfo { get; set; }
        }

        public class transferOnlineCoreInfoInquiryResponse
        {
            public string StatusDescription { get; set; }
        }

        public class transferOnlineThirdPartyInfoInquiryResponse
        {
            public string StatusDescription { get; set; }
            public string InquiryData { get; set; }
        }

        public class transferOnlineInquiryResponse
        {
            public string TraceId { get; set; }
            public string ResponseCode { get; set; }
            public string ResponseDescription { get; set; }
            public transferOnlineCoreInfoInquiryResponse CoreInfo { get; set; }
            public transferOnlineThirdPartyInfoInquiryResponse ThirdPartyInfo { get; set; }
        }
        #endregion

        #region TRANSFER
        public class transferCoreInfo
        {
            public string SourceAccount { get; set; }
            public string Amount { get; set; }
            public string FreeChargeFlag { get; set; }
            public string Remark { get; set; }
        }

        public class transferThirdPartyInfo
        {
            public string DestinationAccount { get; set; }
            public string BankCode { get; set; }
            public string InquiryData { get; set; }
        }

        public class transferOnlinePaymentRequest
        {
            public transferCoreInfo CoreInfo { get; set; }
            public transferThirdPartyInfo ThirdPartyInfo { get; set; }
        }

        public class transferOnlinePaymentResponse
        {
            public string TraceId { get; set; }
            public string ResponseCode { get; set; }
            public string ResponseDescription { get; set; }
            public DateTime TransactionTime { get; set; }
            public CoreInfo CoreInfo { get; set; }
            public ThirdPartyInfo ThirdPartyInfo { get; set; }
        }

        public class CoreInfo
        {
            public string StatusDescription { get; set; }
            public string TransactionReference { get; set; }
        }

        public class ThirdPartyInfo
        {
            public string StatusDescription { get; set; }
            public string ReferenceNumber { get; set; }
        }

        #endregion
    }
}